#!/bin/bash

docker build -t nettematic/jenkins:slave-php7.2 ./slave-php7.2
docker build -t nettematic/jenkins:slave-php7.1 ./slave-php7.1
docker build -t nettematic/jenkins:slave-php7.0 ./slave-php7.0
docker build -t nettematic/jenkins:slave-php5.6 ./slave-php5.6
